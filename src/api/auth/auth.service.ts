import ongletService from 'api/onglet/application/onglet.service'
import {
  getAuth,
  GoogleAuthProvider,
  onAuthStateChanged,
  signInWithPopup,
  signOut,
  User,
} from 'firebase/auth'
import { child, DataSnapshot, get, ref, set } from 'firebase/database'
import { db } from 'firebaseConfig'

const collectionName = `user`

class AuthService {
  public path = ''
  public uid = ''
  user: User | null = null

  constructor() {
    onAuthStateChanged(
      getAuth(),
      (user) => {
        // disconnected
        if (user == null) {
          this.user = null
          this.uid = ''
          return
        }
        // connected
        console.log(`Connected as : ` + user.displayName)
        this.saveUser(user)
        this.user = user
        this.uid = user.uid
        this.path = `${user.uid}/${collectionName}/`
        ongletService.setPath(user.uid)
      },
      (error) => {
        // disconnected
        if (error != null) {
          this.user = null
          this.uid = ''
          return
        }
      }
    )
  }

  signInWithGoogle() {
    const auth = getAuth()
    return signInWithPopup(auth, new GoogleAuthProvider())
  }

  signOut() {
    const auth = getAuth()
    return signOut(auth)
  }

  // ---------------------------------
  getAuth() {
    const auth = getAuth()
    return auth.currentUser
  }

  isAuth() {
    const auth = getAuth()
    return auth.currentUser != null
  }

  getUserUid() {
    const auth = getAuth()
    const uid = auth.currentUser?.uid
    if (uid == null) {
      throw `Non connecté`
    }
    return uid
  }

  private async saveUser(user: User) {
    const { uid } = user
    const isSaved = (await this.getUser(uid)) != null
    if (isSaved) {
      return
    }
    console.log(`saving user`, user)
    const { displayName, phoneNumber, photoURL, email, metadata } = user
    const ref_ = ref(db, this.path)
    return set(ref_, { displayName, phoneNumber, photoURL, email, metadata })
  }

  private async getUser(uid: string) {
    try {
      const snapshot: DataSnapshot = await get(
        child(ref(db), `${uid}/${collectionName}/`)
      )
      return snapshot.val()
    } catch (error) {
      console.error(error)
      return null
    }
  }
}

export default new AuthService()
