import { TransportCommand } from '../../application/transport/transport.command'

export interface Transport extends TransportCommand {
  description: string
  storedLineDirection: string
}
