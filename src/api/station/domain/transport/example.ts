export const type = {
  mode: {
    name: 'Metro',
    icon: 'https://www.ratp.fr/sites/default/files/lines-assets/picto/metro/symbole.1634824971.svg',
  },
  line: {
    value: '7',
    id: 'line:IDFM:C01377',
    label: '7',
    shortName: '7',
    companies: [
      {
        id: 'company:IDFM:100',
        label: 'RATP',
      },
    ],
    network: {
      id: 'network:IDFM:Operator_100',
      label: 'RATP',
    },
    mode: 'Metro',
    modeLabel: 'Metro',
    from: {
      id: 'stop_area:IDFM:73696',
      name: 'La Courneuve - 8 Mai 1945',
      zipCode: '93120',
      city: 'La Courneuve',
    },
    to: {
      id: 'stop_area:IDFM:70488',
      name: "Mairie d'Ivry",
      zipCode: '94200',
      city: 'Ivry-sur-Seine',
    },
    color: 'FA9ABA',
    textColor: '000000',
  },
  stop: {
    value: 'Villejuif Léo Lagrange',
    type: 'StopPoint',
    id: 'stop_point:IDFM:22399',
    x: 602015.6464535334,
    y: 2422882.9082339644,
    name: 'Villejuif Léo Lagrange',
    zipCode: '94800',
    city: 'Villejuif',
    elevator: false,
    stopArea: {
      type: 'StopArea',
      id: 'stop_area:IDFM:70375',
      x: 602010.9896772852,
      y: 2422961.4575166567,
      name: 'Villejuif Léo Lagrange',
      zipCode: '94800',
      city: 'Villejuif',
      elevator: false,
    },
  },
}
