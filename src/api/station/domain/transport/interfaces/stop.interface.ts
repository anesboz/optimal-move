export interface Stop {
  stopArea: { id: string }
  name: string
}
