export interface Mode {
  name: string
  icon: string
}
