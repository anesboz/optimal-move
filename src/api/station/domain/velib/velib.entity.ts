import { VelibCommand } from "../../application/velib/velib.command";

export interface Velib extends VelibCommand {
  description: string
}
