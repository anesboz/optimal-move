import { Transport } from "./transport/transport.entity";
import { Velib } from "./velib/velib.entity";

export type Station = Transport | Velib
