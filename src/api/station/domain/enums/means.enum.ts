export enum EMoyensTransport {
  metro = `Metro`,
  tramway = `Tramway`,
  bus = `Bus`,
  velib = `Velib`,
}
