import { TransportCommand } from "./transport/transport.command";
import { VelibCommand } from "./velib/velib.command";

export type StationCommand = TransportCommand | VelibCommand
