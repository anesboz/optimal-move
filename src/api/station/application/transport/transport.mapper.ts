import { Transport } from 'api/station/domain/transport/transport.entity'
import { TransportCommand } from './transport.command'

export const toModel = (command: TransportCommand): Transport => {
  return { ...command, description: '', storedLineDirection: ''}
}
