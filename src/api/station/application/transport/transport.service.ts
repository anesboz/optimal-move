import { StationService } from '../station.service'
import axios from 'axios'
import _ from 'lodash'
import { obj_groupBy } from 'common/utils/objectManager'

class TransportService extends StationService {
  getTimes(lineId: string, stop_areaID: string) {
    return new Promise((resolve) => {
      const query = `https://api-iv.iledefrance-mobilites.fr/lines/v2/${lineId}/stops/${stop_areaID}/realTime`
      axios
        .get(query)
        .then((res) => {
          const { data } = res.data.nextDepartures
          const grouped = _.groupBy(data, 'lineDirection')
          const result = _.map(grouped, (group) => {
            const lineDirection = group[0].lineDirection
            return { lineDirection, times: _.map(group, 'time') }
          })
          resolve(result)
        })
        .catch((err) => {
          const msg = err.response.data ?? err
          console.log(`🚩 . msg`, msg)
          resolve([])
        })
    })
  }
}

export default new TransportService()
