import { Line } from "api/station/domain/transport/interfaces/line.interface"
import { Mode } from "api/station/domain/transport/interfaces/mode.interface"
import { Stop } from "api/station/domain/transport/interfaces/stop.interface"

export interface TransportCommand {
  mode: Mode
  line: Line
  stop: Stop
}
