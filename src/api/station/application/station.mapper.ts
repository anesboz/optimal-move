import { Station } from '../domain/station.entity'
import { StationCommand } from './station.command'
import { TransportCommand } from './transport/transport.command'
import * as transportMapper from './transport/transport.mapper'
import { VelibCommand } from './velib/velib.command'
import * as velibMapper from './velib/velib.mapper'
import velibService from './velib/velib.service'

export const toModel = (command: StationCommand): Station => {
  if (velibService.isVelib(command)) {
    return velibMapper.toModel(command as VelibCommand)
  }
  return transportMapper.toModel(command as TransportCommand)
}
