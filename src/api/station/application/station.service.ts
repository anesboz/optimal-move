import listService from 'common/utils/list.service'
import pageService from 'api/page/application/page.service'
import { Page } from 'api/page/domain/page.entity'
import { Station } from '../domain/station.entity'
import { StationCommand } from './station.command'
import { toModel } from './station.mapper'

export class StationService {
  create(iOnglet: number, iPage: number, station: StationCommand) {
    const page: Page = pageService.findOne(iOnglet, iPage)

    const newStation = toModel(station)
    page.stations = [...(page.stations ?? []), newStation]
    pageService.updateOne(iOnglet, iPage, page)
  }

  updateOne(
    iOnglet: number,
    iPage: number,
    iStation: number,
    stationPartial: Partial<Station>
  ) {
    const page: Page = pageService.findOne(iOnglet, iPage)
    const newStation = {
      ...page.stations[iStation],
      ...stationPartial,
    }
    const copy = [...page.stations]
    copy.splice(iStation, 1, newStation)
    page.stations = copy

    pageService.updateOne(iOnglet, iPage, page)
  }

  update(iOnglet: number, iPage: number, stations: Station[]) {
    const page: Page = pageService.findOne(iOnglet, iPage)
    page.stations = [...stations]
    pageService.updateOne(iOnglet, iPage, page)
  }

  find(iOnglet: number, iPage: number) {
    return [...pageService.findOne(iOnglet, iPage).stations]
  }

  findOne(iOnglet: number, iPage: number, iStation: number) {
    return this.find(iOnglet, iPage)[iStation]
  }

  getlineDirection(iOnglet: number, iPage: number, iStation: number) {
    return this.find(iOnglet, iPage)[iStation]
  }

  // --------------------------------------

  delete(
    iOnglet: number,
    iPage: number,
    iStation: number,
    callback: () => void
  ) {
    const currentList = this.find(iOnglet, iPage)
    const stations = listService.delete<Station>(currentList, iStation)
    this.update(iOnglet, iPage, stations)
    callback()
  }

  toLeft(
    iOnglet: number,
    iPage: number,
    iStation: number,
    callback: () => void
  ) {
    if (iStation === 0 || iOnglet == null || iPage == null) return
    const currentList = this.find(iOnglet, iPage)
    const stations = listService.toLeft<Station>(currentList, iStation)
    this.update(iOnglet, iPage, stations)
    callback()
  }

  toRight(
    iOnglet: number,
    iPage: number,
    iStation: number,
    callback: () => void
  ) {
    const currentList = this.find(iOnglet, iPage)
    if (
      iStation == currentList.length - 1 ||
      iOnglet == null ||
      iPage == null
    ) {
      return console.log(`Can not move`, { iOnglet, iPage, iStation })
    }
    const stations = listService.toRight<Station>(currentList, iStation)
    this.update(iOnglet, iPage, stations)
    callback()
  }
}

export default new StationService()
