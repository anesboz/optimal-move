import { Station } from 'api/station/domain/station.entity'
import axios from 'axios'
import _ from 'lodash'
import { StationCommand } from '../station.command'
import { StationService } from '../station.service'
import { VelibCommand } from './velib.command'

const OPENDATA_BASE_URL = `https://opendata.paris.fr/api/records/1.0/search/?dataset=`
const MAXLINE = 10000
class VelibService extends StationService {
  async getDispo(stationCode: string) {
    const dataset = `velib-disponibilite-en-temps-reel`
    const url =
      OPENDATA_BASE_URL +
      dataset +
      `&q=stationcode%3D${stationCode}&rows=${MAXLINE}`
    const response = await axios(url)
    return this.getOpendataAttributes(response, [
      `ebike`,
      `mechanical`,
      `numdocksavailable`,
    ])[0]
  }

  async getStationsNames(): Promise<VelibCommand[]> {
    const dataset = `velib-emplacement-des-stations`
    const VELIB_STATIONS_URL =
      OPENDATA_BASE_URL + dataset + `&q=&rows=${MAXLINE}`
    const response = await axios.get(VELIB_STATIONS_URL)
    return this.getOpendataAttributes(response, [`name`, `stationcode`])
  }

  private getOpendataAttributes(response: any, attributes: string[]) {
    return response.data.records.map((e: any) => _.pick(e.fields, attributes))
  }

  isVelib(command: Station | StationCommand) {
    const assumedAsVelibCommand = command as VelibCommand
    return typeof assumedAsVelibCommand.stationcode === 'string'
  }
}

export default new VelibService()
