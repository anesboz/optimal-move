import { Velib } from 'api/station/domain/velib/velib.entity'
import { VelibCommand } from './velib.command'

export const toModel = (command: VelibCommand): Velib => {
  return { ...command, description: '' }
}
