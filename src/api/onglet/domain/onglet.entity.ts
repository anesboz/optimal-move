import { Page } from 'api/page/domain/page.entity'
export interface Onglet {
  name: string
  emoji: string
  // generated
  pages: Page[]
}
