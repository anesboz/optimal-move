import { Onglet } from '../domain/onglet.entity'
import { OngletCommand } from './onglet.command'

export const toModel = (command: OngletCommand): Onglet => {
  return { pages: [], ...command }
}
