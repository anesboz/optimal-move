import pageService from "api/page/application/page.service"
import listService from "common/utils/list.service"
import { ongletActions } from "common/redux/reducers/onglet"
import store from "common/redux/store"
import { ref, set } from "firebase/database"
import { db } from "firebaseConfig"
import { Onglet } from "../domain/onglet.entity"
import { OngletCommand } from "./onglet.command"
import { toModel } from "./onglet.mapper"
import lsService from "common/utils/localStrorage/ls.service"
import { LsKeys } from "common/utils/localStrorage/ls.enum"

const collectionName = `onglets` ?? []
class OngletService {
  create(doc: OngletCommand) {
    const onglet: Onglet = toModel(doc)
    const newOnglets = [...this.find(), onglet]
    this.update(newOnglets)
    // create one page
    pageService.create(newOnglets.length - 1, { name: new Date().toString() })
  }

  update(data: Onglet[]) {
    const ref_ = ref(db, this.getPath())
    return set(ref_, data)
  }

  updateOne(iOnglet: number, ongletPartial: Partial<Onglet>) {
    const data = this.find()
    data.splice(iOnglet, 1, { ...data[iOnglet], ...ongletPartial })
    this.update(data)
  }

  setOnglets(onglets: Onglet[]) {
    store.dispatch(ongletActions.set(onglets))
    lsService.save(LsKeys.ONGLETS, onglets)
  }

  refresh() {
    store.dispatch(ongletActions.refresh())
  }

  find() {
    return [...store.getState().onglet.onglets]
  }

  findOne(index: number) {
    return { ...this.find()[index] }
  }

  // getIOnglet() {
  //   return store.getState().onglet.iOnglet
  // }

  setIOnglet(iOnglet: number) {
    store.dispatch(ongletActions.setIOnglet(iOnglet))
    lsService.save(LsKeys.LAST_I_ONGLET, iOnglet)
  }

  // specific firebase

  setPath(uid: string) {
    const payload = `${uid}/${collectionName}/`
    store.dispatch(ongletActions.setPath(payload))
  }

  getPath() {
    return store.getState().onglet.path
  }

  // --------------------------------------

  delete(index: number, callback: () => void) {
    const onglets = this.find()
    const data = listService.delete<Onglet>(onglets, index)
    this.update(data)
    callback()
  }

  toLeft(index: number | null, callback: () => void) {
    if (index === 0 || index == null) return
    const data = listService.toLeft<Onglet>(this.find(), index)
    this.update(data)
    callback()
  }

  toRight(index: number | null, callback: () => void) {
    const list = this.find()
    if (index == list.length - 1 || index == null) return
    const data = listService.toRight<Onglet>(list, index)
    this.update(data)
    callback()
  }
}

export default new OngletService()
