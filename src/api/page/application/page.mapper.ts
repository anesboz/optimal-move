import { Page } from '../domain/page.entity'
import { PageCommand } from './page.command'
import { v4 as uuid } from 'uuid'

export const toModel = (command: PageCommand): Page => {
  return { ...command, name: '', description: '', stations: [] }
}
