import listService from 'common/utils/list.service'
import ongletService from 'api/onglet/application/onglet.service'
import { Onglet } from 'api/onglet/domain/onglet.entity'
import { ongletActions } from 'common/redux/reducers/onglet'
import store from 'common/redux/store'
import { Page } from '../domain/page.entity'
import { PageCommand } from './page.command'
import { toModel } from './page.mapper'

class PageService {
  create(iOnglet: number, page: PageCommand) {
    const onglet: Onglet = ongletService.findOne(iOnglet)

    const newPage: Page = toModel(page)
    onglet.pages = [...(onglet.pages ?? []), newPage]
    ongletService.updateOne(iOnglet, onglet)
  }

  updateOne(iOnglet: number, iPage: number, pagePartial: Partial<Page>) {
    const onglet: Onglet = ongletService.findOne(iOnglet)

    const pages = [...onglet.pages]

    pages.splice(iPage, 1, { ...pages[iPage], ...pagePartial })
    onglet.pages = pages
    ongletService.updateOne(iOnglet, onglet)
  }

  update(iOnglet: number, pages: Page[]) {
    if (iOnglet === null) {
      throw `iOnglet can not be null`
    }
    const onglet: Onglet = ongletService.findOne(iOnglet)

    onglet.pages = pages
    ongletService.updateOne(iOnglet, onglet)
  }

  find(iOnglet: number) {
    return [...ongletService.findOne(iOnglet).pages]
  }

  findOne(iOnglet: number, iPage: number) {
    return { ...this.find(iOnglet)[iPage] }
  }

  getIPage() {
    return store.getState().onglet.iPage
  }

  setIPage(iPage: number) {
    store.dispatch(ongletActions.setIPage(iPage))
  }

  // --------------------------------------

  delete(iOnglet: number, iPage: number, callback: () => void) {
    const currentList = this.find(iOnglet)
    const pages = listService.delete<Page>(currentList, iPage)
    this.update(iOnglet, pages)
    callback()
  }

  toLeft(iOnglet: number, iPage: number, callback: () => void) {
    if (iPage === 0 || iOnglet == null) return
    const pages = listService.toLeft<Page>(this.find(iOnglet), iPage)
    this.update(iOnglet, pages)
    callback()
  }

  toRight(iOnglet: number, iPage: number, callback: () => void) {
    const list = this.find(iOnglet)
    if (iPage == list.length - 1 || iOnglet == null) return
    const pages = listService.toRight<Page>(list, iPage)
    this.update(iOnglet, pages)
    callback()
  }
}

export default new PageService()
