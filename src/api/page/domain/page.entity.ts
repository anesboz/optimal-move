import { Station } from 'api/station/domain/station.entity'

export interface Page {
  name: string
  description: string
  stations: Station[]
}
