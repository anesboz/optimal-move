import ongletService from "api/onglet/application/onglet.service"
import pageService from "api/page/application/page.service"
import { useAppSelector } from "common/redux/store"
import { useNavigate } from "react-router-dom"
import MoreMenu from "views/components/MoreMenu"
// confirmation box
import store from "common/redux/store"
import { mainActions } from "common/redux/reducers/main"
import ConfirmBox from "views/components/ConfirmBox/ConfirmBox"

export default function PageMenu() {
  const { onglets, iOnglet, iPage } = useAppSelector((state) => state.onglet)
  const navigate = useNavigate()
  const setIPage = pageService.setIPage
  const setIOnglet = ongletService.setIOnglet

  const openDialog = (elment: JSX.Element) =>
    store.dispatch(mainActions.openDialog(elment))

  return (
    <MoreMenu
      list={[
        {
          name: `Page to Left`,
          icon: `keyboard_arrow_left`,
          action: () =>
            pageService.toLeft(
              iOnglet,
              iPage,
              () => iOnglet != null && setIPage(iPage - 1)
            ),
        },
        {
          name: `Page to Right`,
          icon: `keyboard_arrow_right`,
          action: () =>
            pageService.toRight(
              iOnglet,
              iPage,
              () => iOnglet != null && setIPage(iPage + 1)
            ),
          /* ongletService.toRight(
              iOnglet,
              () => iOnglet != null && setIOnglet(iOnglet + 1)
            )
             */
        },
        {
          name: `Page delete`,
          icon: `delete`,
          action: () => {
            openDialog(
              <ConfirmBox
                action={() => {
                  const newIdex = iPage - 1 >= 0 ? iPage - 1 : 0
                  pageService.delete(iOnglet, iPage, () => setIPage(newIdex))
                }}
              />
            )
          },
        },

        // --------------------------------------------------
        {
          name: `devider`,
          icon: "",
          action: () => null,
        },
        // --------------------------------------------------
        {
          name: `Onglet to left`,
          icon: "keyboard_double_arrow_left",
          action: () => {
            ongletService.toLeft(
              iOnglet,
              () => iOnglet != null && setIOnglet(iOnglet - 1)
            )
          },
        },
        {
          name: `Onglet to right`,
          icon: "keyboard_double_arrow_right",
          action: () => {
            ongletService.toRight(
              iOnglet,
              () => iOnglet != null && setIOnglet(iOnglet + 1)
            )
          },
        },

        {
          name: `Onglet edit`,
          icon: "edit",
          action: () => {
            if (iOnglet == null) return
            navigate(`/add-onglet`, {
              state: { onglet: onglets[iOnglet], iOnglet },
            })
          },
        },
        {
          name: `Onglet delete`,
          icon: "delete",
          action: () => {
            openDialog(
              <ConfirmBox
                action={() => {
                  const newIOnglet = iOnglet - 1 >= 0 ? iOnglet - 1 : 0
                  ongletService.delete(iOnglet, () => setIOnglet(newIOnglet))
                }}
              />
            )
          },
        },
      ]}
    />
  )
}
