import { Grid, Icon, IconButton } from "@mui/material"
import { useAppSelector } from "common/redux/store"
import { Page } from "api/page/domain/page.entity"
import { Station } from "api/station/domain/station.entity"
import { useNavigate } from "react-router-dom"
import TextFieldSynchro from "views/components/TextSynchro"
import pageService from "api/page/application/page.service"
import PageMenu from "./sub/PageMenu"
import StationComponent from "../Station/Station"

export default function PageC() {
  const { onglets, iOnglet, iPage } = useAppSelector((state) => state.onglet)
  const page: Page = (onglets[iOnglet]?.pages ?? [])?.[iPage]
  const navigate = useNavigate()
  const setIPage = pageService.setIPage

  const { name, description } = page
  const stations = page.stations ?? []

  return (
    <Grid container>
      <Grid container className="center" sx={{ position: "relative" }}>
        <Grid item className="">
          <TextFieldSynchro
            value={name}
            callback={(name: string) =>
              pageService.updateOne(iOnglet, iPage, { name })
            }
            placeholder={`page ${iPage + 1}`}
          />
        </Grid>
        <Grid item sx={{ position: "absolute", right: 5, top: 5 }}>
          <PageMenu />
        </Grid>
      </Grid>
      <Grid container className="center-x">
        {stations.map((station: Station, i) => {
          return (
            <Grid container className="center-x " key={i}>
              <StationComponent station={station} iStation={i} />
            </Grid>
          )
        })}
        <Grid container className="center-x" sx={{ my: 3 }}>
          <IconButton
            sx={{
              my: 2,
              // backgroundColor: MBLUE,
              //  color: 'white'
            }}
            onClick={() => {
              navigate("/add-station", { state: { iOnglet, iPage } })
            }}
          >
            <Icon>add</Icon>
          </IconButton>
        </Grid>
      </Grid>
    </Grid>
  )
}
