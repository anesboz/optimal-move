import ToggleButton from "@mui/material/ToggleButton"
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup"
import { useNavigate } from "react-router-dom"
import ongletService from "api/onglet/application/onglet.service"
import { useAppSelector } from "common/redux/store"
import { Grid } from "@mui/material"
import { MGRAY } from "common/variables/styles"

export default function Onglets() {
  const { onglets, iOnglet } = useAppSelector((state) => state.onglet)
  const setIOnglet = ongletService.setIOnglet

  const navigate = useNavigate()

  return (
    <Grid
      sx={{
        overflowX: `scroll`,
        border: `1px solid ${MGRAY}`,
        backgroundColor: "white",
      }}
      container
    >
      <ToggleButtonGroup
        color="primary"
        value={iOnglet}
        exclusive
        onChange={(event, newValue: number) => {
          if (newValue === null) {
            return
          }
          setIOnglet(newValue)
        }}
        className=""
      >
        {[...onglets, { id: "ajouter", name: "", emoji: "➕" }].map(
          ({ name, emoji }, i, arr) => {
            const ARRET_SIZE = 70
            const isAddButton = i === arr.length - 1
            return (
              <ToggleButton
                key={i}
                value={i}
                sx={{
                  height: ARRET_SIZE,
                  width: ARRET_SIZE,
                  borderTop: "none",
                }}
                onClick={() => {
                  ongletService.refresh()
                  if (!isAddButton) return
                  navigate(`/add-onglet`)
                }}
              >
                {name} {emoji}
              </ToggleButton>
            )
          }
        )}
      </ToggleButtonGroup>
    </Grid>
  )
}
