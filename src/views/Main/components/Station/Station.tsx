import { Station } from 'api/station/domain/station.entity'
import velibService from 'api/station/application/velib/velib.service'
import TransportComponent from './Transport'
import VelibComponent from './Velib'

export default function StationComponent(props: {
  station: Station
  iStation: number
}) {
  const { station } = props

  const isVelib = velibService.isVelib(station)
  return (
    <>
      {isVelib ? (
        <VelibComponent {...props} />
      ) : (
        <TransportComponent {...props} />
      )}
    </>
  )
}
