import stationService from 'api/station/application/station.service'
import { Station } from 'api/station/domain/station.entity'
import store, { useAppSelector } from 'common/redux/store'
import { useEffect, useState } from 'react'
import { transportModes } from 'views/AddStation/sub/ModeSetting'
import Case from './sub/Case'
import StationMenu from './sub/StationMenu'
import TextFieldSynchro from 'views/components/TextSynchro'
import { mainActions } from 'common/redux/reducers/main'
import { Transport } from 'api/station/domain/transport/transport.entity'
import transportService from 'api/station/application/transport/transport.service'

interface IData {
  lineDirection: string
  times: string[]
}

export default function TransportComponent(props: {
  station: Station
  iStation: number
}) {
  const { iStation } = props
  const station = props.station as Transport

  const { iOnglet, iPage, lastRefresh } = useAppSelector(
    (state) => state.onglet
  )

  const { description, mode, line, stop, storedLineDirection } = station

  const [data, setData] = useState<IData[]>([])

  useEffect(() => {
    setData([])
    const delayDebounceFn = setTimeout(async () => {
      const lineID = line?.id
      const stop_areaID = stop?.stopArea?.id
      if (lineID == null || stop_areaID == null) {
        return console.log(`Information manquantes`)
      }

      const times = (await transportService.getTimes(
        lineID,
        stop_areaID
      )) as IData[]
      setData(times)
    }, 0)
    // no spam
    return () => clearTimeout(delayDebounceFn)
  }, [line, stop, lastRefresh])

  const selected_ = data.find(
    ({ lineDirection }) => lineDirection === storedLineDirection
  )
  const unknownLineDirection = data.length > 0 && selected_ == null
  const selected = selected_ ?? data[0] ?? {}
  const lineDirection = selected.lineDirection
  const times = selected.times ?? loadingList

  return (
    <div style={{ height: `3.5rem`, margin: `0.6rem 0`, width: '100%' }}>
      <div
        style={{
          marginLeft: '1rem',
          fontSize: `55%`,
          color: unknownLineDirection ? 'red' : `gray`,
          marginBottom: `5px`,
          height: `20%`,
          cursor: 'pointer',
        }}
        onClick={() => {
          const currentIndex = data.findIndex(
            ({ lineDirection }) => lineDirection === storedLineDirection
          )
          const newIDirection = (currentIndex + 1) % data.length
          const newStoredLineDirection = data[newIDirection].lineDirection
          stationService.updateOne(iOnglet, iPage, iStation, {
            storedLineDirection: newStoredLineDirection,
          })
          // setUnknownLineDirection(false)
        }}
      >
        {stop?.name} ➙ {lineDirection}
        {/* {`${stop.name} ➙ ${lineDirection}`} */}
      </div>
      {/* times */}
      <div
        style={{
          height: `80%`,
          overflowX: `scroll`,
          whiteSpace: `nowrap`,
        }}
        className=""
      >
        <Case
          element={
            <img
              style={{ height: `70%` }}
              src={transportModes
                .find((e) => e.name === mode?.name)
                ?.lineIcon(line.label)}
              onClick={() =>
                store.dispatch(
                  mainActions.openDialog(
                    <TextFieldSynchro
                      value={description}
                      callback={(description: string) =>
                        stationService.updateOne(iOnglet, iPage, iStation, {
                          description,
                        })
                      }
                    />
                  )
                )
              }
            />
          }
        />
        {times?.map((time, j) => (
          <Case
            element={
              <span
                style={{
                  color: comming.list.includes(time) ? comming.color : '',
                }}
              >
                {time}
              </span>
            }
            key={j}
          />
        ))}
        <Case element={<StationMenu iStation={iStation} station={station} />} />
      </div>
    </div>
  )
}

const comming = { list: ['0', '1', '2'], color: '#f6501e' }
const loadingList = [...Array(2).keys()].map(() => `...`)
