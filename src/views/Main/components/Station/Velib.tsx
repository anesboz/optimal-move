import stationService from 'api/station/application/station.service'
import { Station } from 'api/station/domain/station.entity'
import store, { useAppSelector } from 'common/redux/store'
import { useEffect, useState } from 'react'
import Case from './sub/Case'
import StationMenu from './sub/StationMenu'
import TextFieldSynchro from 'views/components/TextSynchro'
import { mainActions } from 'common/redux/reducers/main'
import { Velib } from 'api/station/domain/velib/velib.entity'
import velibLogo from 'common/assets/images/velib/velibLogo.png'
import velibE from 'common/assets/images/velib/velibE.png'
import velibM from 'common/assets/images/velib/velibM.png'
import velibP from 'common/assets/images/velib/velibP.png'
import velibService from 'api/station/application/velib/velib.service'
import { Grid } from '@mui/material'

interface IVelibData {
  ebike: number
  mechanical: number
  numdocksavailable: number
}

export default function VelibComponent(props: {
  station: Station
  iStation: number
}) {
  const { iStation } = props
  const station = props.station as Velib

  const { iOnglet, iPage, lastRefresh } = useAppSelector(
    (state) => state.onglet
  )

  const { name, stationcode, description } = station

  const [data, setData] = useState<IVelibData>({} as IVelibData)

  useEffect(() => {
    const delayDebounceFn = setTimeout(async () => {
      const dispos = await velibService.getDispo(stationcode)
      setData(dispos)
    }, 500)
    // no spam
    return () => clearTimeout(delayDebounceFn)
  }, [lastRefresh])

  return (
    <div style={{ height: `3.5rem`, margin: `0.6rem 0`, width: '100%' }}>
      <div
        style={{
          marginLeft: '1rem',
          fontSize: `55%`,
          color: `gray`,
          marginBottom: `5px`,
          height: `20%`,
          cursor: 'pointer',
        }}
      >
        {`${name}`}
      </div>
      {/* times */}
      <div
        style={{
          height: `80%`,
          overflowX: `scroll`,
          whiteSpace: `nowrap`,
        }}
        className=""
      >
        <Case
          velib
          element={
            <img
              style={{ height: `70%` }}
              src={velibLogo}
              onClick={() =>
                store.dispatch(
                  mainActions.openDialog(
                    <TextFieldSynchro
                      value={description}
                      callback={(description: string) =>
                        stationService.updateOne(iOnglet, iPage, iStation, {
                          description,
                        })
                      }
                    />
                  )
                )
              }
            />
          }
        />
        {[
          { src: velibE, nb: data.ebike },
          { src: velibM, nb: data.mechanical },
          { src: velibP, nb: data.numdocksavailable },
        ].map(({ src, nb }, i) => (
          <Case
            key={i}
            element={
              <Grid sx={{ height: `100%` }} className="center">
                <img
                  style={{ height: `70%`, display: `inline-block` }}
                  src={src}
                />
                &nbsp;&nbsp;
                {nb}
              </Grid>
            }
            velib
          />
        ))}

        <Case element={<StationMenu iStation={iStation} station={station} />} />
      </div>
    </div>
  )
}
