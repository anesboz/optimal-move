import BusAlertIcon from '@mui/icons-material/BusAlert'

export const emptyData = [...new Array(2)].map((e, i) => ({
  message: (
    <div style={{ color: '#00000059' }} key={i}>
      <BusAlertIcon />
      <span style={{ fontSize: '60%' }}>&nbsp;no data</span>
    </div>
  ),
}))

export default function Case(props: { element: JSX.Element; velib?: boolean }) {
  const { element, velib } = props
  return (
    <div style={{ ...style(velib) }}>
      <div className="center" style={{ height: `100%`, width: `100%` }}>
        {element}
      </div>
    </div>
  )
}

const style = (velib = false) => ({
  display: `inline-block`,
  // position: `relative`,
  height: `90%`,
  width: velib ? '24%' : `32%`,
  border: `1px solid #dddddd`,
  margin: `0 0.1rem`,
  padding: `0 0.5rem`,
  // color: [...isHere, ...isComing].includes(element) ? 'orange' : 'black',
  overflow: `hidden`,
})
