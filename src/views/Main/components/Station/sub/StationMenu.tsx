import ongletService from 'api/onglet/application/onglet.service'
import stationService from 'api/station/application/station.service'
import { Station } from 'api/station/domain/station.entity'
import { useAppSelector } from 'common/redux/store'
import { useNavigate } from 'react-router-dom'
import MoreMenu from 'views/components/MoreMenu'

export default function StationMenu(props: {
  iStation: number
  station: Station
}) {
  const { iOnglet, iPage } = useAppSelector((state) => state.onglet)
  const navigate = useNavigate()
  const setIOnglet = ongletService.setIOnglet

  const { iStation, station } = props

  return (
    <MoreMenu
      list={[
        {
          name: `Move Up`,
          icon: `keyboard_double_arrow_up`,
          action: () =>
            stationService.toLeft(iOnglet, iPage, iStation, () => null),
        },
        {
          name: `Move down`,
          icon: `keyboard_double_arrow_down`,
          action: () =>
            stationService.toRight(iOnglet, iPage, iStation, () => null),
        },
        {
          name: `Duplicate`,
          icon: `content_copy`,
          action: () => stationService.create(iOnglet, iPage, station),
        },
        {
          name: `Delete`,
          icon: `delete`,
          action: () => {
            stationService.delete(iOnglet, iPage, iStation, () => null)
          },
        },
        {
          name: `Edit`,
          icon: 'edit',
          action: () => {
            if (iOnglet == null) return
            navigate(`/add-station`, {
              state: { iOnglet, iPage, iStation, station },
            })
          },
        },
      ]}
    />
  )
}
