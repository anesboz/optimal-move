import { Grid } from "@mui/material"
import ongletService from "api/onglet/application/onglet.service"
import { Onglet } from "api/onglet/domain/onglet.entity"
import { useAppSelector } from "common/redux/store"
import { getAuth } from "firebase/auth"
import { onValue, ref } from "firebase/database"
import { db } from "firebaseConfig"
import React from "react"
import Onglets from "./components/Onglets"
import { Icon, IconButton, Pagination } from "@mui/material"
import pageService from "api/page/application/page.service"
import PageC from "./components/Page/PageC"

export default function Main() {
  const { path } = useAppSelector((state) => state.onglet)
  React.useEffect(() => {
    const { currentUser } = getAuth()
    if (currentUser == null) return
    const ref_ = ref(db, ongletService.getPath())
    onValue(ref_, (snapshot) => {
      const onglets: Onglet[] = snapshot.exists()
        ? Object.values(snapshot.val())
        : []
      ongletService.setOnglets(onglets)
    })
  }, [path])

  const { onglets, iOnglet, iPage } = useAppSelector((state) => state.onglet)
  const pages = onglets[iOnglet]?.pages ?? []

  return (
    <Grid item xs={12} md={6}>
      <Grid container className="center-x">
        <Grid container sx={{ overflowY: `scroll`, maxHeight: "50vh" }}>
          {pages.map((page, i) => (i === iPage ? <PageC key={i} /> : null))}
        </Grid>
        <Grid container sx={{ position: "fixed", bottom: 0 }}>
          <Grid
            container
            className="center"
            sx={{
              visibility: onglets.length > 0 ? `visible` : `hidden`,
              mb: 4,
            }}
          >
            <Pagination
              count={pages.length}
              page={iPage + 1}
              onChange={(event, newValue) => pageService.setIPage(newValue - 1)}
            />
            <IconButton
              onClick={() => {
                pageService.create(iOnglet, { name: new Date().toString() })
                pageService.setIPage(pages.length)
              }}
            >
              <Icon>add</Icon>
            </IconButton>
          </Grid>
          <Onglets />
        </Grid>
      </Grid>
    </Grid>
  )
}
