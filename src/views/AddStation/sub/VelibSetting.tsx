import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { useEffect, useState } from 'react'
import { Grid } from '@mui/material'
import {
  MAccordion,
  MAccordionDetails,
  MAccordionSummary,
} from 'views/components/MAccordion/MAccordionColored'
import { COLORS_SAMPLE, ratpBLUE } from '../common/constants'
import SearchBarV from 'views/components/SearchBarV/SearchBarV'
import velibService from 'api/station/application/velib/velib.service'
import { Mode } from 'api/station/domain/transport/interfaces/mode.interface'
import { VelibCommand } from 'api/station/application/velib/velib.command'
import { EMoyensTransport } from 'api/station/domain/enums/means.enum'

export default function VelibSetting(props: {
  order: number
  mode: Mode
  velibHook: any
  expandedHook: any
}) {
  const { order, mode, velibHook, expandedHook } = props
  const [velibCommand, setVelibCommand] = velibHook
  const [expanded, setExpanded] = expandedHook

  const [filters, setFilters] = useState<any>([])
  const disabled = mode.name != EMoyensTransport.velib

  useEffect(() => {
    if (disabled) return
    velibService
      .getStationsNames()
      .then((stations) => {
        const options = stations.map(({ name, stationcode }) => ({
          key: stationcode,
          value: name,
        }))

        setFilters(options)
      })
      .catch((err) => {
        const msg = err.response.data ?? err
        console.log(`🚩 . msg`, msg)
      })
  }, [mode])

  return (
    <Grid container marginTop={3}>
      <Grid item sx={{ width: 10, bgcolor: COLORS_SAMPLE[order] }}></Grid>
      <Grid item flex={1}>
        <MAccordion
          expanded={expanded === order}
          onChange={() => {
            setExpanded(expanded === order ? null : order)
          }}
          disabled={disabled}
        >
          <MAccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography
              color={velibCommand.name === null ? 'red' : ''}
              className="center-y"
            >
              {`Depuis la station `}
            </Typography>
            &nbsp;
            {renderChosenStop(velibCommand)}
          </MAccordionSummary>
          <MAccordionDetails style={{ textAlign: 'center' }}>
            <SearchBarV
              // keyName={`stop`}
              multiple={false}
              filters={filters}
              setSelected={(newValue) => {
                const paylaod = newValue?.[0] ?? {}
                const velibCommand: VelibCommand = {
                  name: paylaod.value,
                  stationcode: paylaod.key,
                }
                setVelibCommand(velibCommand)
                if (Object.keys(paylaod).length === 0) return
                setExpanded(order + 1)
              }}
              // others
              title={`Sélectionner une station`}
              renderRow={renderRow}
              // just to fix
              keyName={undefined}
              refreshFun={undefined}
              updateFilters={undefined}
              disabled={undefined}
            />
          </MAccordionDetails>
        </MAccordion>
      </Grid>
    </Grid>
  )
}

const renderRow = (e: VelibCommand) => {
  if (e.stationcode == null) return null
  return (
    <small>
      {`${e.stationcode}`}
      &nbsp;
      <span style={{ opacity: 0.6, fontSize: `70%` }}>{`📍 ${e.name}`}</span>
    </small>
  )
}

const renderChosenStop = (e: VelibCommand) => {
  if (e.stationcode == null) return null
  return (
    <div className="center-y">
      <small
        style={{
          padding: 10,
          backgroundColor: ratpBLUE,
          color: `white`,
          fontWeight: `150%`,
        }}
      >
        {`${e.stationcode}`}
      </small>
      &nbsp;
      <span style={{ opacity: 0.6, fontSize: `70%` }}>{`📍 ${e.name}`}</span>
    </div>
  )
}
