export const assetsURL = `https://www.ratp.fr/sites/default/files/lines-assets/picto/`

export const COLORS_SAMPLE = [
  `#5555ff`,
  `#ff5555`,
  `#00aa00`,
  `#55ffff`,
  `#ff55ff`,
]

export const ratpBLUE = `#18143c`