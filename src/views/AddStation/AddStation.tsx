import { useEffect, useState } from 'react'
import ModeSetting from './sub/ModeSetting'
import LineSetting from './sub/LineSetting'
import StopSetting from './sub/StopSetting'
import {
  Button,
  FormControlLabel,
  FormGroup,
  Grid,
  Switch,
} from '@mui/material'
import { useLocation, useNavigate } from 'react-router-dom'
import { renderDEVcomment } from './common/utils'
import stationService from 'api/station/application/station.service'
import { Mode } from 'api/station/domain/transport/interfaces/mode.interface'
import { Line } from 'api/station/domain/transport/interfaces/line.interface'
import { Stop } from 'api/station/domain/transport/interfaces/stop.interface'
import StationComponent from 'views/Main/components/Station/Station'
import VelibSetting from './sub/VelibSetting'
import { VelibCommand } from 'api/station/application/velib/velib.command'

export default function AddStation() {
  const navigate = useNavigate()
  const { state } = useLocation()
  const { iOnglet, iPage, iStation, station } = state ?? {}
  const [devMode, setDevMode] = useState(false)
  const [expanded, setExpanded] = useState(0)
  const [description, setDescription] = useState('')
  const [mode, setMode] = useState<Mode>({} as Mode)
  const [line, setLine] = useState<Line>({} as Line)
  const [stop, setStop] = useState<Stop>({} as Stop)
  const [editMode, setEditMode] = useState(false)

  const [velibCommand, setVelibCommand] = useState<VelibCommand>(
    {} as VelibCommand
  )

  const isVelib = mode.name === 'Velib'

  const newStation = { description, mode, line, stop }
  const disabled = iOnglet == undefined || iPage == undefined

  useEffect(() => {
    if ([iOnglet, iPage, iStation, station].some((e) => e == null)) {
      return
    }
    setEditMode(true)
    const { description, mode, line, stop } = station
    setDescription(description)
    setMode(mode)
    setLine(line)
    setStop(stop)
  }, [state])

  return (
    <Grid container>
      <Grid container justifyContent={`flex-end`} padding={1}>
        <FormGroup>
          <FormControlLabel
            control={
              <Switch
                checked={devMode}
                onChange={(event) => setDevMode(event.target.checked)}
              />
            }
            label="Mode développeur"
          />
        </FormGroup>
      </Grid>
      <ModeSetting
        order={0}
        modeHook={[mode, setMode]}
        expandedHook={[expanded, setExpanded]}
        devMode={devMode}
      />
      {isVelib ? (
        <VelibSetting
          order={1}
          mode={mode}
          velibHook={[velibCommand, setVelibCommand]}
          expandedHook={[expanded, setExpanded]}
        />
      ) : (
        <>
          <LineSetting
            order={1}
            mode={mode}
            lineHook={[line, setLine]}
            expandedHook={[expanded, setExpanded]}
            devMode={devMode}
          />
          <StopSetting
            order={2}
            lineId={line.id}
            stopHook={[stop, setStop]}
            expandedHook={[expanded, setExpanded]}
            devMode={devMode}
          />
        </>
      )}

      <Grid item justifyContent={`center`} sx={{ margin: `2rem auto` }}>
        <Button
          variant="contained"
          color="success"
          onClick={() => {
            if (isVelib) {
              stationService.create(
                iOnglet,
                iPage,
                JSON.parse(JSON.stringify(velibCommand))
              )
              return navigate('/')
            }

            const paylaod = JSON.parse(JSON.stringify(newStation))
            if (editMode) {
              stationService.updateOne(iOnglet, iPage, iStation, paylaod)
              setEditMode(false)
            } else {
              stationService.create(iOnglet, iPage, paylaod)
            }
            navigate('/')
          }}
          disabled={disabled}
        >
          Ajouter
        </Button>
      </Grid>
      <Grid
        container
        className="center"
        sx={{
          display: disabled && !devMode ? '' : 'none',
          width: '100%',
          fontSize: '80%',
          color: 'gray',
          textAlign: 'center',
          mx: `2rem`,
        }}
      >
        {`Pour pouvoir ajouter une station il faut passer par le boutton "plus" dans une page`}
      </Grid>
      {!devMode ? null : (
        <>
          {renderDEVcomment({
            devMode,
            title: `Horaires en temps réel`,
            content: (
              <>
                <>
                  <span>{`https://api-iv.iledefrance-mobilites.fr/lines/v2/`}</span>
                  <span style={{ color: line.id == null ? `red` : `orange` }}>
                    {line.id ?? `NULL`}
                  </span>
                  <span>{`/stops/`}</span>
                  <span
                    style={{
                      color: stop.stopArea?.id == null ? `red` : `orange`,
                    }}
                  >
                    {stop.stopArea?.id ?? `NULL`}
                  </span>
                  <span>{`/realTime`}</span>
                </>
              </>
            ),
          })}

          <StationComponent
            station={{ ...newStation, storedLineDirection: '' }}
            iStation={0}
          />
        </>
      )}
    </Grid>
  )
}
