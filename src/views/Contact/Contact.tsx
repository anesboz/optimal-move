import { Grid } from '@mui/material'
import githubLogo from 'common/assets/logos/github.png'
import gitlabLogo from 'common/assets/logos/gitlab.png'
import gmailLogo from 'common/assets/logos/gmail.png'
import { APPLICATION_NAME } from 'common/variables/constants'

const contact = {
  name: `Anes BOUZOUAOUI`,
  email: `anesbouzouaoui@gmail.com`,
  gitlab: `https://gitlab.com/anesboz`,
  github: `https://github.com/anesboz`,
}
export default function Contact() {
  const dim = 24
  return (
    <Grid container className="center">
      <Grid item sx={{ p: 5 }}>
        <h2>Contact Us</h2>
        <p>
          {APPLICATION_NAME} a été developpé par <b>{contact.name}</b>
        </p>
        <ul>
          <li className="center-y">
            <img src={gmailLogo} style={{ height: dim, marginRight: `1rem` }} />
            <a
              href={'mailto:' + contact.email}
              target="_blank"
              rel="noopener noreferrer"
            >
              Gmail
            </a>
          </li>
          <br></br>
          <li className="center-y">
            <img
              src={githubLogo}
              style={{ height: dim, marginRight: `1rem` }}
            />
            <a href={contact.github} target="_blank" rel="noopener noreferrer">
              GitHub
            </a>
          </li>
          <br></br>
          <li className="center-y">
            <img
              src={gitlabLogo}
              style={{ height: dim, marginRight: `1rem` }}
            />
            <a href={contact.gitlab} target="_blank" rel="noopener noreferrer">
              Gitlab
            </a>
          </li>
        </ul>
      </Grid>
    </Grid>
  )
}
