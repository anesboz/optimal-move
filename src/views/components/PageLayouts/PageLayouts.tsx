import { useLocation } from 'react-router-dom'
import { routes } from 'routes'
import MBreadcrumb from './sub/MBreadcrumb'
import SideDrawer from './sub/SideDrawer'
import TopBar from './sub/TopBar'

export default function PageLayouts() {
  const location = useLocation()
  const route = routes.find((e) => e.path === location.pathname)
  const isHomePage = route?.path === '/'

  return (
    <>
      <TopBar isHomePage={isHomePage} />
      <MBreadcrumb route={route} isHomePage={isHomePage} />
      <SideDrawer />
    </>
  )
}
