import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import {
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material'
import Icon from '@mui/material/Icon'
import { getAuth } from 'firebase/auth'
import { routes } from 'routes'
import store, { useAppSelector } from 'common/redux/store'
import { mainActions } from 'common/redux/reducers/main'
import { useNavigate } from 'react-router-dom'

const DRAWER_WIDTH = 240

export default function SideDrawer() {
  const { open, element } = useAppSelector((state) => state.main.drawer)
  const toggleDrawer = () => store.dispatch(mainActions.openDrawer(!open))

  const navigate = useNavigate()


  const userDisplayName = getAuth().currentUser?.displayName
  return (
    <Box component="nav">
      <Drawer
        // container={container}
        variant="temporary"
        open={open}
        onClose={toggleDrawer}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: DRAWER_WIDTH,
          },
        }}
      >
        <ListItem disablePadding>
          <ListItemButton>
            <ListItemIcon>
              <Icon>person</Icon>
            </ListItemIcon>
            <ListItemText primary={userDisplayName} />
          </ListItemButton>
        </ListItem>
        <Divider />
        <Box>
          <List>
            {routes
              .filter((e) => e.sideMenu === true)
              .map(({ name, path, icon, isPublic }) => {
                // not connected
                if (isPublic != true && userDisplayName == null) {
                  return null
                }
                return (
                  <ListItem
                    key={name}
                    disablePadding
                    onClick={() => {
                      navigate(path)
                      toggleDrawer()
                    }}
                  >
                    <ListItemButton>
                      <ListItemIcon>
                        <Icon>{icon}</Icon>
                      </ListItemIcon>
                      <ListItemText primary={name} />
                    </ListItemButton>
                  </ListItem>
                )
              })}
          </List>
          <Divider />
        </Box>
        {element}
      </Drawer>
    </Box>
  )
}
