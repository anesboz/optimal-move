import Breadcrumbs from '@mui/material/Breadcrumbs'
import Link from '@mui/material/Link'
import HomeIcon from '@mui/icons-material/Home'
import { APPLICATION_NAME } from 'common/variables/constants'
import { useNavigate } from 'react-router-dom'
import { RouteI } from 'routes'
import { Icon } from '@mui/material'

export default function MBreadcrumb(props: {
  route: RouteI | undefined
  isHomePage: boolean
}) {
  const { route, isHomePage } = props
  const { name, path, icon } = route ?? {}
  const navigate = useNavigate()
  return (
    <Breadcrumbs aria-label="breadcrumb" sx={{ m: 2 }}>
      <Link
        underline="hover"
        sx={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}
        color="inherit"
        // href="/"
        onClick={() => navigate('/')}
      >
        <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
        {APPLICATION_NAME}
      </Link>
      {path == null || isHomePage ? null : (
        <Link
          underline="hover"
          sx={{ display: 'flex', alignItems: 'center', cursor: 'pointer' }}
          color="inherit"
          onClick={() => navigate(path)}
        >
          <Icon sx={{ mr: 0.5 }} fontSize="inherit">
            {icon}
          </Icon>
          {name}
        </Link>
      )}
    </Breadcrumbs>
  )
}
