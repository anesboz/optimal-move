import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import { routes } from 'routes'
import Icon from '@mui/material/Icon'
import { APPLICATION_NAME } from 'common/variables/constants'
import { AppBar } from '@mui/material'
import store, { useAppSelector } from 'common/redux/store'
import { mainActions } from 'common/redux/reducers/main'
import { useNavigate } from 'react-router-dom'

export default function TopBar(props: { isHomePage: boolean }) {
  const { isHomePage } = props
  const { open } = useAppSelector((state) => state.main.drawer)
  const toggleDrawer = () => store.dispatch(mainActions.openDrawer(!open))
  const navigate = useNavigate()

  const PAD = 20
  return (
    <AppBar component="nav" position="relative">
      <Toolbar>
        <Box
          sx={{
            position: `absolute`,
            left: PAD,
          }}
        >
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={toggleDrawer}
          >
            <MenuIcon />
          </IconButton>
        </Box>

        <Button
          sx={{ color: `white`, margin: `auto` }}
          onClick={() => {
            if (isHomePage === true) {
              return navigate(0)
            }
            navigate('/')
          }}
          startIcon={<Icon>{routes[0].icon}</Icon>}
        >
          <Typography variant="h6" component="span" sx={{ cursor: `pointer` }}>
            {APPLICATION_NAME}
          </Typography>
        </Button>

        <Box
          sx={{
            position: `absolute`,
            right: PAD,
          }}
        >
          {routes
            .filter((e) => e.topMenu === true)
            .map(({ name, path, icon }) => {
              return (
                <IconButton
                  key={name}
                  color="inherit"
                  aria-label="open drawer"
                  edge="end"
                  onClick={() => navigate(path)}
                >
                  <Icon>{icon}</Icon>
                </IconButton>
              )
            })}
        </Box>

        {/* <Button
          startIcon={<Icon>{icon}</Icon>}
          key={name}
          sx={{ color: 'white' }}
          onClick={() => navigate(path)}
        >
          <Typography
            sx={{
              display: { xs: 'none', md: 'block' },
            }}
          >
            {name}
          </Typography>
        </Button> */}
      </Toolbar>
    </AppBar>
  )
}
