import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { getAuth, onAuthStateChanged } from 'firebase/auth'

export default function RoutePrivate(props: { children: JSX.Element }) {
  const { children } = props
  const navigate = useNavigate()
  const auth = getAuth()
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    const AuthCheck = onAuthStateChanged(auth, (user) => {
      setLoading(false)
      // disconnected
      if (user == null) {
        navigate('/login')
        return
      }
    })

    return () => AuthCheck()
  }, [auth])

  // if (loading) return <p>loading ...</p>

  return children
}
