import * as React from 'react'
import Box from '@mui/material/Box'
import SpeedDial from '@mui/material/SpeedDial'
import SpeedDialIcon from '@mui/material/SpeedDialIcon'
import SpeedDialAction from '@mui/material/SpeedDialAction'
import { Icon } from '@mui/material'

interface Action {
  name: string
  action: () => any
}

type Position = 'right' | 'left'

export default function SpeedDialM(props: {
  actions: Action[]
  icon: string
  pos: Position
}) {
  const { actions, icon, pos } = props
  const [open, setOpen] = React.useState(false)
  return (
    <SpeedDial
      sx={{ position: 'absolute', bottom: 120, [pos]: 100 }}
      ariaLabel={Math.random() * 10000 + ''}
      open={open}
      onClick={() => setOpen(!open)}
      icon={
        <SpeedDialIcon
          openIcon={<Icon>{icon}</Icon>}
          icon={<Icon>{icon}</Icon>}
        />
      }
    >
      {actions.map(({ name, action }, i) => {
        return (
          <SpeedDialAction
            key={i}
            icon={<Icon>bolt</Icon>}
            tooltipTitle={name}
            tooltipOpen
            onClick={() => {
              console.log(`${name} action triggered`)
              action()
            }}
          />
        )
      })}
    </SpeedDial>
  )
}
