import Typography from '@mui/material/Typography'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { Grid } from '@mui/material'
import { Dispatch, SetStateAction } from 'react'
import {
  MAccordion,
  MAccordionDetails,
  MAccordionSummary,
} from './MAccordionColored'

export default function MyOrdredAccrodion(props: {
  order: number
  state: number | null
  setState: Dispatch<SetStateAction<number | null>>
  title?: string
  error?: boolean
  disabled?: boolean
  content?: JSX.Element
}) {
  const { order, title, error, disabled, content, state, setState } = props
  const COLORS = [`#5555ff`, `#ff5555`, `#00aa00`, `#55ffff`, `#ff55ff`]
  const color = COLORS[order]
  const [expanded, setExpanded] = [state, setState]
  return (
    <Grid container marginTop={3}>
      <Grid item sx={{ width: 10, bgcolor: color }}></Grid>
      <Grid item flex={1}>
        <MAccordion
          expanded={expanded === order}
          onChange={() => {
            setExpanded(expanded === order ? null : order)
          }}
          disabled={disabled}
        >
          <MAccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography color={error ? 'red' : ''} className="center-y">
              {title}
            </Typography>
          </MAccordionSummary>
          <MAccordionDetails style={{ textAlign: 'center' }}>
            {content}
          </MAccordionDetails>
        </MAccordion>
      </Grid>
    </Grid>
  )
}
