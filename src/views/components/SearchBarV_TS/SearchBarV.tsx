/* eslint-disable no-prototype-builtins */
/* eslint-disable react/display-name */
import * as React from 'react'
import Autocomplete from '@mui/material/Autocomplete'
import { Avatar, Button, InputAdornment, useAutocomplete } from '@mui/material'
import { createFilterOptions } from '@mui/material/Autocomplete'
import { Chip, Grid } from '@mui/material'
import { useEffect, useRef } from 'react'
import SendIcon from '@mui/icons-material/Send'
import { useState } from 'react'
// common subFolder
import loader from './common/utils/loader.gif'
import {
  ListboxComponent,
  StyledPopper,
  TextFieldWrapper,
} from './common/components'
import { RefreshList } from './common/RefreshList'
import { Option, SBV_I } from './common/interfaces'

// npm i @mui/lab @mui/material react-window notistack

export default function SearchBarV({
  options = [], // must contain value
  setSelected,
  updateOptions_whenTyping,
  renderRow,
  error = false,
  disabled,
  title = 'Filtrer sur',
  placeholder = 'Tapez ...',
  helperText = `Incorrect entry.`,
}: SBV_I) {
  const [localSelected, setLocalSelected] = useState<Option[]>([])
  const [lastSelected, setLastSelected] = useState<Option[]>([])

  const isUpdateMode = updateOptions_whenTyping != null

  const [searchTerm, setSearchTerm] = useState('')
  const [loading, setLoading] = useState(false)

  const height = 30

  // focus
  const inputRef = useRef<any>()
  useEffect(() => {
    if (loading || disabled) return
    inputRef?.current?.focus()
  }, [loading])

  return (
    <Grid
      container
      justifyContent="center"
      sx={{ opacity: disabled ? 0.1 : 1 }}
    >
      <Grid item justifyContent="center" flex={1}>
        {/* name VALUE extra */}
        <Autocomplete
          // virtual
          loading={loading}
          disableListWrap
          PopperComponent={StyledPopper}
          ListboxComponent={ListboxComponent}
          ListboxProps={
            { renderRow } as ReturnType<
              ReturnType<typeof useAutocomplete>['getListboxProps']
            >
          }
          // renderOption={(props, option) => [props, option]}
          //
          isOptionEqualToValue={(option, value) => {
            return value?.value === option?.value
          }}
          fullWidth
          size="small"
          autoHighlight
          filterOptions={createFilterOptions({
            stringify: (option) => {
              if (isUpdateMode) return searchTerm
              return Object.values(option).join('')
            },
            trim: true,
          })}
          multiple
          options={options.map((e) => ({ ...e, renderRow }))}
          value={localSelected}
          onChange={(event, newValue) => {
            setLocalSelected(newValue)
            setSelected(newValue)
          }}
          getOptionLabel={(option: Option) => `` + option.value ?? ``}
          renderTags={(values, getTagProps) => {
            return values.flatMap((option: Option, index) =>
              option == null ? (
                []
              ) : (
                <Chip
                  label={
                    <div className="center">
                      {renderRow(option) ?? defaultChip(option)}
                    </div>
                  }
                  {...getTagProps({ index })}
                />
              )
            )
          }}
          renderInput={(params: any) => {
            const rest = {
              ...params,
              value: isUpdateMode ? searchTerm : params.value,
            }
            return (
              <TextFieldWrapper
                {...rest}
                inputRef={inputRef}
                label={title}
                placeholder={placeholder}
                inputProps={{
                  ...params.inputProps,
                  disabled,
                  style: { height },
                }}
                error={error}
                helperText={disabled ? null : error ? helperText : null}
                InputProps={{
                  ...params.InputProps,
                  startAdornment: (
                    <>
                      <InputAdornment
                        position="start"
                        style={{
                          visibility:
                            disabled || !loading ? `hidden` : `visible`,
                          display:
                            disabled || (!loading && !isUpdateMode)
                              ? `none`
                              : undefined,
                        }}
                      >
                        <Avatar src={loader} />
                      </InputAdornment>
                      {params.InputProps.startAdornment}
                    </>
                  ),
                }}
                onChange={(event) => {
                  const value = event.target.value
                  if (
                    !isUpdateMode ||
                    value == null ||
                    value?.trim().length < 1 ||
                    disabled
                  ) {
                    return
                  }
                  setSearchTerm(value)
                  setLoading(true)
                  const delay = setTimeout(() => {
                    updateOptions_whenTyping(value?.trim()).finally(() => {
                      setLoading(false)
                    })
                  }, 1000)
                  return () => clearTimeout(delay)
                }}
              />
            )
          }}
        />
      </Grid>
    </Grid>
  )
}

// renderRow(value)

function defaultChip(option: Option) {
  return (
    <div>
      <small
        style={{
          display: `inline-block`,
          color: `gray`,
        }}
      >{`value:`}</small>
      <span>{` ${option.value}`}</span>
    </div>
  )
}
