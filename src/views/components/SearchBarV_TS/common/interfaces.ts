export interface Option {
  key: string
  value: string
}

export interface SBV_I {
  /**
   * un identifiant qui permet de stocker la valeur du champs
   *
   * dans LocalStorage
   */
  keyName: string
  /**
   * communiquée depuis le parent
   *
   * list de type Option
   */
  options: Option[]
  /**
   * si définie : un button refresh s'affiche permet de update les filtres
   */
  updateOptions_byClickOnRefresh?: () => Promise<Option[]>
  /**
   * si définie : en recherchant dans la bar de recherche
   * les filtres vont update automatiquement (cas d'API par exemple)
   */
  updateOptions_whenTyping: (searchWord: string) => Promise<Option[]>

  /**
   * communiquée depuis le parent
   *
   * setState sur la valeur recherché
   */
  setSelected: (value: Option[] | Option) => void

  /**
   * En cherchant dans l'autocomplete, il faut cliquer sur
   * un boutton "valider" pour changer le setSelected
   * * @default false
   */
  validateMode: boolean

  /**
   * communiquée depuis le parent
   *
   * indique c'est la valeur attendue est une liste ou un object
   * @default true
   */
  multiple: boolean
  /**
   * précise comment un filter sera affichée en html
   */
  renderRow: (option: Option) => JSX.Element
  error: boolean
  disabled: boolean
  title: string
  placeholder: string
  helperText: string
}
