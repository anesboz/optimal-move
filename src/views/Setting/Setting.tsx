import {
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Grid
} from '@mui/material'
import Icon from '@mui/material/Icon'
// confirmation box
import store from 'common/redux/store'
import { mainActions } from 'common/redux/reducers/main'
import ConfirmBox from 'views/components/ConfirmBox/ConfirmBox'
import lsService from 'common/utils/localStrorage/ls.service'
import List from '@mui/material/List'

export default function Setting() {

  const openDialog = (elment: JSX.Element) =>
    store.dispatch(mainActions.openDialog(elment))

  return (
    <Grid container className='center' sx={{ p: 5 }} >
      <List sx={{ width: '100%' }} >
        <ListItem onClick={() => { openDialog(<ConfirmBox action={lsService.clear} />) }} disablePadding>
          <ListItemButton >
            <ListItemIcon>
              <Icon>delete_forever</Icon>
            </ListItemIcon>
            <ListItemText className='center-x' primary={`Supprimer le cache`} />
          </ListItemButton>
        </ListItem>
      </List>
    </Grid>

  )
}
