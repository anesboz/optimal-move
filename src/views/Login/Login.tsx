import { useState, useEffect } from 'react'
import { getAuth, onAuthStateChanged } from 'firebase/auth'
import { useNavigate } from 'react-router-dom'
import { Button, Grid } from '@mui/material'
import { useSnackbar } from 'notistack'
import authService from 'api/auth/auth.service'
import omLogo from 'common/assets/images/omLogo.png'
import Icon from '@mui/material/Icon'

export default function Login() {
  const navigate = useNavigate()
  const [isAuth, setIsAuth] = useState(false)
  const [loading, setLoading] = useState(false)
  const auth = getAuth()
  useEffect(() => {
    const AuthCheck = onAuthStateChanged(auth, (user) => {
      setIsAuth(user != null)
    })
    return () => AuthCheck()
  }, [auth])

  const { enqueueSnackbar } = useSnackbar()

  const currentUser = getAuth().currentUser

  return (
    <Grid container sx={{ height: 500 }}>
      <Grid item xs={12} className="center">
        <img
          src={omLogo}
          style={{
            maxHeight: '100px',
            width: '80%'
          }}
        />
      </Grid>
      <Grid item xs={12} className="center">
        {isAuth ? (
          <Grid container className="center">
            <Grid
              container
              className="center"
              sx={{ m: 3 }}
            // style={{ width: `100%` }}
            >{`Connected as : ${currentUser?.displayName}`}</Grid>
            <Button
              variant="contained"
              color="error"
              onClick={authService.signOut}
              disabled={loading}
            >
              Log out
            </Button>
          </Grid>
        ) : (
          <Button
            variant="contained"
            startIcon={<Icon>add_to_drive</Icon>}
            onClick={signInWithGoogle}
            disabled={loading}
            color="success"
          >
            Continue with Google
          </Button>
        )}
      </Grid>
    </Grid>
  )

  function signInWithGoogle() {
    setLoading(true)
    authService
      .signInWithGoogle()
      .then(() => {
        navigate('/')
        // setTimeout(() => {
        //   window.location.reload()
        // }, 1000);
      })
      .catch((error) => {
        console.log(error)
      })
      .finally(() => setLoading(false))
  }
}
