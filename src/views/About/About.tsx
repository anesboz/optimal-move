import { Grid } from '@mui/material'
import { APPLICATION_NAME } from 'common/variables/constants'

export default function About() {
  return (
    <Grid container sx={{ margin: `auto`, m: 10 }} className="center">
      <b>{APPLICATION_NAME}</b>&nbsp;
      {` est une application en React TypeScript`}
    </Grid>
  )
}
