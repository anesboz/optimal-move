import { Button, Grid, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'

export default function NotFound() {
  const navigate = useNavigate()
  return (
    <div style={{ margin: `auto` }} className="center">
      <Typography sx={{ fontSize: `30vh`, color: `gray`, display: `block` }}>
        \(o_o)/
      </Typography>

      <Typography
        sx={{ m: 5 }}
      >{`Le lien que vous cherchez n'existe pas (404)`}</Typography>
      <Grid item xs={12} className="center">
        <Button
          onClick={() => navigate('/')}
          variant="contained"
          color="primary"
          sx={{ m: `auto` }}
        >
          Go HOME
        </Button>
      </Grid>
    </div>
  )
}
