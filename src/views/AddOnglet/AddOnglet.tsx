import { useState, useEffect } from "react"
import { Button, Grid, TextField } from "@mui/material"
import { useLocation, useNavigate } from "react-router-dom"
import MyOrdredAccrodion from "views/components/MAccordion/MyOrdredAccrodion"
import ongletService from "api/onglet/application/onglet.service"

export default function AddOnglet() {
  const navigate = useNavigate()
  const { state } = useLocation()

  const defaultEmoji = "⭐"
  const [name, setName] = useState("")
  const [emoji, setEmoji] = useState(defaultEmoji)
  const [index, setIndex] = useState<number | null>(null)

  const isEditMode = index != null

  useEffect(() => {
    const { onglet, iOnglet } = state ?? {}
    // editMode
    if (onglet != null) {
      const { name, emoji } = onglet
      setName(name)
      setEmoji(emoji)
      setIndex(iOnglet)
    }
  }, [state])

  const [expanded, setExpanded] = useState<number | null>(0)

  const props1 = {
    order: 0,
    state: expanded,
    setState: setExpanded,
    title: `Onglet`,
    content: (
      <Grid container spacing={2}>
        <Grid item flex={1}>
          <TextField
            fullWidth
            label="Emoji (facultatif)"
            autoComplete="off"
            inputProps={{ autoComplete: "new-password" }}
            onChange={(event) => setEmoji(event.target.value ?? "")}
            value={emoji ?? ""}
            required
          />
        </Grid>
        <Grid item flex={1}>
          <TextField
            fullWidth
            label="Nom (facultatif)"
            autoComplete="off"
            inputProps={{ autoComplete: "new-password" }}
            onChange={(event) => setName(event.target.value ?? "")}
            value={name ?? ""}
            placeholder={`Mon nouvel onglet`}
            required
          />
        </Grid>
      </Grid>
    ),
  }

  return (
    <Grid container>
      <MyOrdredAccrodion {...props1} />
      <Grid item justifyContent={`center`} sx={{ margin: `2rem auto` }}>
        <Button
          variant="contained"
          color="success"
          sx={{ minWidth: 150 }}
          disabled={[name, emoji].every((e) => e == null || e.length === 0)}
          onClick={() => {
            if (isEditMode) {
              ongletService.updateOne(index, { name, emoji })
            } else {
              ongletService.create({ name, emoji })
            }
            navigate("/")
          }}
        >
          {isEditMode ? `Mettre à jour` : `Save`}
        </Button>
      </Grid>
    </Grid>
  )
}
