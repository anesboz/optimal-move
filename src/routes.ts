import { APPLICATION_NAME } from 'common/variables/constants'
import About from 'views/About/About'
import Login from 'views/Login/Login'
import Contact from 'views/Contact/Contact'
import Setting from 'views/Setting/Setting'
import Main from './views/Main/Main'
import AddStation from 'views/AddStation/AddStation'
import AddOnglet from 'views/AddOnglet/AddOnglet'

export const basename = 'optimal-move/'

export interface RouteI {
  name: string
  path: string
  icon: string
  element: React.FunctionComponent
  isPublic?: boolean
  sideMenu?: boolean
  topMenu?: boolean
}

export const routes: RouteI[] = [
  {
    name: APPLICATION_NAME,
    path: `/`,
    icon: `directions_bus_filled`,
    element: Main,
    sideMenu: true,
  },
  {
    name: `About`,
    path: `/about`,
    icon: `info`,
    element: About,
    sideMenu: true,
    isPublic: true,
  },
  {
    name: `Contact`,
    path: `/contact`,
    icon: `forward_to_inbox`,
    element: Contact,
    sideMenu: true,
    isPublic: true,
  },
  {
    name: `Ajouter une station`,
    path: `/add-station`,
    icon: `gps_fixed_icon`,
    element: AddStation,
  },
  {
    name: `Ajouter un onglet`,
    path: `/add-onglet`,
    icon: `turned_in_icon`,
    element: AddOnglet,
  },
  {
    name: `Setting`,
    path: `/setting`,
    icon: `settings`,
    element: Setting,
    topMenu: true,
  },
  {
    name: `Login`,
    path: `/login`,
    icon: `login`,
    element: Login,
    sideMenu: true,
    isPublic: true,
  },
]
