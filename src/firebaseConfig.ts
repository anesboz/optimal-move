import { initializeApp } from 'firebase/app'
import { getDatabase } from 'firebase/database'

const firebaseConfig = {
  apiKey: 'AIzaSyAEphUyclFq2d82r2qQJdYZWV-XulA348g',
  authDomain: 'optimal-move-firebase.firebaseapp.com',
  databaseURL:
    'https://optimal-move-firebase-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'optimal-move-firebase',
  storageBucket: 'optimal-move-firebase.appspot.com',
  messagingSenderId: '533743375335',
  appId: '1:533743375335:web:fcd7fa13344c9a7069a342',
  measurementId: 'G-S9K6LKFXNN',
}

const app = initializeApp(firebaseConfig)
export const db = getDatabase(app)
