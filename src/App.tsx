import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'
import { Grid, ThemeProvider } from '@mui/material'
import { Provider } from 'react-redux'
import store from 'common/redux/store'
import { mTheme } from 'common/variables/styles'
import { basename, RouteI, routes } from 'routes'
import NotFound from './views/NotFound/NotFound'
import RoutePrivate from 'views/components/RoutePrivate/RoutePrivate'
import PageLayouts from 'views/components/PageLayouts/PageLayouts'
import MDialog from 'views/components/MDialog'

function App() {
  return (
    <Provider store={store}>
      <SnackbarProvider>
        <ThemeProvider theme={mTheme}>
          <BrowserRouter basename={basename}>
            <PageLayouts />
            <MDialog />
            <Grid
              container
              sx={{ position: `relative`, p: 3, pt: 0 }}
              className="center"
            >
              <Routes>
                {routes.map((route: RouteI, i) => {
                  const Child = route.element
                  const Element =
                    route.isPublic === true ? (
                      <Child />
                    ) : (
                      <RoutePrivate>
                        <Child />
                      </RoutePrivate>
                    )
                  return <Route key={i} path={route.path} element={Element} />
                })}
                <Route path={'*'} element={<NotFound />} />
              </Routes>
            </Grid>
          </BrowserRouter>
        </ThemeProvider>
      </SnackbarProvider>
    </Provider>
  )
}

export default App
