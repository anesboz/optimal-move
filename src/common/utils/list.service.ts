class listService {
  toLeft<Type>(list: Array<Type>, index: number) {
    const elem = list[index]
    // delete in current position
    list.splice(index, 1)
    list.splice(index - 1, 0, elem)
    return list
  }

  toRight<Type>(list: Array<Type>, index: number) {
    const elem = list[index]
    // delete in current position
    list.splice(index, 1)
    list.splice(index + 1, 0, elem)
    return list
  }

  delete<Type>(list: Array<Type>, index: number) {
    if (list.length > 0) list.splice(index, 1)
    return list
  }
}

export default new listService()
