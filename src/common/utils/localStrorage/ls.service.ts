import { LsKeys } from './ls.enum'

interface LS {
  value: unknown
  date: Date
}

class LsService {
  save(key: LsKeys, value: unknown) {
    const payload: LS = { value, date: new Date() }
    localStorage.setItem(key, JSON.stringify(payload))
  }

  load(key: LsKeys): LS {
    const brut = localStorage.getItem(key) ?? '{}'
    const { value, date } = JSON.parse(brut) as LS
    const date_ = date == null ? new Date() : new Date(date)
    return { value, date: date_ }
  }

  removeItem(key: LsKeys) {
    localStorage.removeItem(key)
  }

  clear() {
    localStorage.clear()
  }
}

export default new LsService()
