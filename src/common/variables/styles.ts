import { createTheme } from '@mui/material'

export const mTheme = createTheme({
  breakpoints: {},
})

export const mobileBreakPoints = {
  xs: 12,
  md: 6,
}


export const MGRAY = '#80808073'
export const MBLUE = '#1976d2'
