import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { Onglet } from "api/onglet/domain/onglet.entity"
import { LsKeys } from "common/utils/localStrorage/ls.enum"
import lsService from "common/utils/localStrorage/ls.service"

interface IOnglet {
  path: string
  onglets: Onglet[]
  iOnglet: number
  iPage: number
  lastRefresh: number
}

const lastIOnglet = lsService.load(LsKeys.LAST_I_ONGLET).value as number
const cachedOnglets = lsService.load(LsKeys.ONGLETS).value as Onglet[]

// Define the initial state using that type
const initialState: IOnglet = {
  path: "",
  onglets: cachedOnglets ?? [],
  iOnglet: lastIOnglet ?? 0,
  iPage: 0,
  lastRefresh: 0,
}

export const ongletSlice = createSlice({
  name: "onglet",
  initialState,
  reducers: {
    setPath: (state, action: PayloadAction<string>) => {
      state.path = action.payload
    },
    set: (state, action: PayloadAction<Onglet[]>) => {
      state.onglets = action.payload
    },
    setIOnglet: (state, action: PayloadAction<number>) => {
      state.iOnglet = action.payload
      state.iPage = 0
    },
    setIPage: (state, action: PayloadAction<number>) => {
      state.iPage = action.payload
    },
    refresh: (state) => {
      state.lastRefresh = new Date().getTime()
    },
  },
})

// Action creators are generated for each case reducer function
export const ongletActions = ongletSlice.actions

export const ongletReducer = ongletSlice.reducer
